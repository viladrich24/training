SHELL=/bin/bash

.PHONY: install
install:
	docker-compose build

.PHONY: stop
stop:
	docker-compose stop

.PHONY: start
start:
	docker-compose up -d --no-build --remove-orphans --force-recreate
	docker-compose exec -u $(shell id -u ${USER}):$(shell id -g ${USER}) php composer install -o --prefer-dist
	docker-compose exec -u $(shell id -u ${USER}):$(shell id -g ${USER}) php /wait
	docker-compose exec -u $(shell id -u ${USER}):$(shell id -g ${USER}) php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration
	docker-compose exec -u $(shell id -u ${USER}):$(shell id -g ${USER}) php bin/console doctrine:database:create --if-not-exists --env=test
	docker-compose exec -u $(shell id -u ${USER}):$(shell id -g ${USER}) php bin/console doctrine:migrations:migrate --no-interaction --env=test --allow-no-migration

.PHONY: restart
restart: stop start

.PHONY: shell
shell:
	docker exec -it training_php_1 bash