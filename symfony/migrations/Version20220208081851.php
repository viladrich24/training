<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208081851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE itinerary (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE itinerary_activities (itinerary_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_46A49CBD15F737B2 (itinerary_id), INDEX IDX_46A49CBD81C06096 (activity_id), PRIMARY KEY(itinerary_id, activity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_activities ADD CONSTRAINT FK_46A49CBD15F737B2 FOREIGN KEY (itinerary_id) REFERENCES itinerary (id)');
        $this->addSql('ALTER TABLE itinerary_activities ADD CONSTRAINT FK_46A49CBD81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE itinerary_activities DROP FOREIGN KEY FK_46A49CBD15F737B2');
        $this->addSql('DROP TABLE itinerary');
        $this->addSql('DROP TABLE itinerary_activities');
        $this->addSql('ALTER TABLE activity CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE solutions solutions VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
