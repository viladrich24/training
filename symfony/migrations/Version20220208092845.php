<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208092845 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, students_itinerary INT DEFAULT NULL, INDEX IDX_B723AF33D3E7691A (students_itinerary), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student_activity (student_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_1FFF45C1CB944F1A (student_id), INDEX IDX_1FFF45C181C06096 (activity_id), PRIMARY KEY(student_id, activity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33D3E7691A FOREIGN KEY (students_itinerary) REFERENCES itinerary (id)');
        $this->addSql('ALTER TABLE student_activity ADD CONSTRAINT FK_1FFF45C1CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE student_activity ADD CONSTRAINT FK_1FFF45C181C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student_activity DROP FOREIGN KEY FK_1FFF45C1CB944F1A');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE student_activity');
        $this->addSql('ALTER TABLE activity CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE solutions solutions VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
