<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208094745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE student_activity');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF33D3E7691A');
        $this->addSql('DROP INDEX IDX_B723AF33D3E7691A ON student');
        $this->addSql('ALTER TABLE student ADD activity_id INT DEFAULT NULL, CHANGE students_itinerary itinerary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3315F737B2 FOREIGN KEY (itinerary_id) REFERENCES itinerary (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3381C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
        $this->addSql('CREATE INDEX IDX_B723AF3315F737B2 ON student (itinerary_id)');
        $this->addSql('CREATE INDEX IDX_B723AF3381C06096 ON student (activity_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE student_activity (student_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_1FFF45C181C06096 (activity_id), INDEX IDX_1FFF45C1CB944F1A (student_id), PRIMARY KEY(student_id, activity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE student_activity ADD CONSTRAINT FK_1FFF45C1CB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE student_activity ADD CONSTRAINT FK_1FFF45C181C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE activity CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE solutions solutions VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF3315F737B2');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF3381C06096');
        $this->addSql('DROP INDEX IDX_B723AF3315F737B2 ON student');
        $this->addSql('DROP INDEX IDX_B723AF3381C06096 ON student');
        $this->addSql('ALTER TABLE student ADD students_itinerary INT DEFAULT NULL, DROP itinerary_id, DROP activity_id');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF33D3E7691A FOREIGN KEY (students_itinerary) REFERENCES itinerary (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B723AF33D3E7691A ON student (students_itinerary)');
    }
}
