<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220208152207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF3381C06096');
        $this->addSql('DROP INDEX IDX_B723AF3381C06096 ON student');
        $this->addSql('ALTER TABLE student ADD name VARCHAR(255) NOT NULL, CHANGE activity_id current_activity_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF333F14CB4F FOREIGN KEY (current_activity_id) REFERENCES activity (id)');
        $this->addSql('CREATE INDEX IDX_B723AF333F14CB4F ON student (current_activity_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activity CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE solutions solutions VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF333F14CB4F');
        $this->addSql('DROP INDEX IDX_B723AF333F14CB4F ON student');
        $this->addSql('ALTER TABLE student DROP name, CHANGE current_activity_id activity_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3381C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B723AF3381C06096 ON student (activity_id)');
    }
}
