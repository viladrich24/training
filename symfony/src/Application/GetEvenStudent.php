<?php

namespace App\Application;

use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GetEvenStudent
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository){
        $this->studentRepository = $studentRepository;
    }

    public function exec(int $studentId): ?Student
    {
        if($studentId % 2 != 0){
            throw new HttpException(404, "Student Not Found because is Odd");
        }
        return $this->studentRepository->findStudent($studentId + 1);
    }
}