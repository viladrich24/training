<?php

namespace App\Domain\Activity;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity()
* @ORM\Table(name="activity")
*/
class Activity{

    const MIN_DIFFICULTY = 1;
    const MAX_DIFFICULTY = 10;

    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    private int $id;
    /**
    * @ORM\Column(type="string", length=255)
    */
    private string $name;
    /**
    * @ORM\Column(type="integer")
    */
    private int $position;
    /**
     * @ORM\Column(type="integer")
     */
    private int $difficulty;
    /**
    * @ORM\Column(type="integer")
    */
    private int $time;
    /**
    * @ORM\Column(type="string", length=255)
    */
    private string $solutions;

    public function __construct(int $id, string $name, int $position, int $difficulty, int $time, string $solutions){

        if($difficulty < self::MIN_DIFFICULTY || $difficulty > self::MAX_DIFFICULTY){
            throw new   \InvalidArgumentException('Difficulty must be set between 1-10');
        }
        $this->id = $id;
        $this->name = $name;
        $this->position = $position;
        $this->difficulty = $difficulty;
        $this->time = $time;
        $this->solutions = $solutions;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getName(): string{
        return $this->name;
    }

    public function getPosition(): int{
        return $this->position;
    }

    public function getDifficulty(): int{
        return $this->difficulty;
    }

    public function getTime(): int{
        return $this->time;
    }

    public function getSolutions(): string{
        return $this->solutions;
    }

    public function getScore(string $answers): int{

        $correctAnswers = 0;
        $arrayAnswers = explode("_", $answers);
        $arraySolutions = explode("_", $this->solutions);

        $numSolutions = sizeOf($arraySolutions);
        $numAnswers = sizeOf($arrayAnswers);

        if($numSolutions != $numAnswers){
            throw new InvalidNumberOfAnswersException("The number of answers doesn't match the number of solutions");
        }

        for ($i = 0; $i < $numSolutions; $i++) {
            if ($arraySolutions[$i] == $arrayAnswers[$i]) {
                $correctAnswers++;
            }
        }
        return $correctAnswers / $numSolutions * 100;
    }
}

