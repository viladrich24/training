<?php

namespace App\Domain\Itinerary;

use App\Domain\Activity\Activity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="itinerary")
 */
class Itinerary{

    const MAX_ACTIVITIES = 5;
    const MAX_DIFFICULTY = 10;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="App\Domain\Activity\Activity")
     * @ORM\JoinTable(name="itinerary_activities",
     *      joinColumns={@ORM\JoinColumn(name="itinerary_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")}
     *      )
     */
    private array $activities;

    private array $orderedTags;

    public function __construct(int $id, string $name, array $activities)
    {
        if(empty($activities)){
            throw new \InvalidArgumentException('ActivitiesArray is Empty');
        }

        foreach($activities as $activity){
            if(!$activity instanceof Activity){
                throw new \InvalidArgumentException('ActivitiesArray is not filled with Activities');
            }
        }

        $this->id = $id;
        $this->name = $name;
        $this->activities = $activities;
        $this->createOrderedArray();
    }

    public function getId(): int{
        return $this->id;
    }

    public function getItineraryName(): String{
        return $this->name;
    }

    public function getActivity(int $id): Activity{
        return $this->activities[$id-1];
    }

    public function getActivities(): array{
        return $this->activities;
    }

    public function getTag(Activity $activity): int{
        return $activity->getDifficulty()*10 + $activity->getPosition();
    }

    private function compareTag(array $orderedTags, int $comparingTag, int $nextTag): int {

        foreach($orderedTags as $index=>$tag){
            if($tag == $comparingTag && isset($orderedTags[$index+1])){
                $nextTag = $orderedTags[$index+1];
                break;
            }
        }
        return $nextTag;
    }

    public function createOrderedArray() {
        foreach($this->activities as $activity){

            $this->activities[strval($this->getTag($activity))] = $activity;
            $orderedTags[] = $this->getTag($activity);
        }

        sort($orderedTags, SORT_NATURAL | SORT_FLAG_CASE);
        $this->orderedTags = $orderedTags;
    }

    public function nextActivity(Activity $currentActivity, string $answer, int $timeDoingExerciseInSecs, Activity $lastCompletedActivity): ?Activity{

        $nextTag = $this->getTag($currentActivity);
        $lastTag = $this->getTag($lastCompletedActivity);
        $score = $currentActivity->getScore($answer);
        $activities = $this->getActivities();
        $orderedTags = $this->orderedTags;

        if($score >= 75) {
            if($timeDoingExerciseInSecs < ($currentActivity->getTime() / 2)) {
                $nextTag = $this->getNextTag($nextTag, $orderedTags, $currentActivity);
            }
            else {
                $nextTag = $this->compareTag($orderedTags, $nextTag, $nextTag);
            }
        }

        elseif ($score < 20) {
            if($nextTag != $orderedTags[0]) {
                $nextTag = $this->compareTag($orderedTags, $lastTag, $nextTag);
            }
        }

        return $activities[strval($nextTag)];
    }

    public function getNextTag(int $nextTag, array $orderedTags, Activity $currentActivity)
    {
        $diffTag = $nextTag / 10;
        $diffTag = ceil($diffTag) * 10;
        $maxTag = $diffTag + 9;

        foreach ($orderedTags as $tag) {
            if ($tag > $diffTag && $tag < $maxTag) {
                $nextTag = $tag;
                $maxTag = $tag;
            }
        }

        if ($nextTag == $this->getTag($currentActivity)) {
            $nextTag = $this->compareTag($orderedTags, $nextTag, $nextTag);
        }
        return $nextTag;
    }
}