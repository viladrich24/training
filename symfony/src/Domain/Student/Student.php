<?php

namespace App\Domain\Student;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="student")
 */
class Student{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $studentName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Itinerary\Itinerary")
     * @ORM\JoinColumn(name="itinerary_id", referencedColumnName="id")
     */
    private Itinerary $itinerary;
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Activity\Activity")
     * @ORM\JoinColumn(name="current_activity_id", referencedColumnName="id")
     */
    private Activity $currentActivity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Activity\Activity")
     * @ORM\JoinColumn(name="last_activity_id", referencedColumnName="id")
     */
    private Activity $lastActivity;

    public function __construct(int $id, string $studentName, Itinerary $itinerary, Activity $currentActivity, Activity $lastActivity)
    {
        $this->id = $id;
        $this->studentName = $studentName;
        $this->itinerary = $itinerary;
        $this->currentActivity = $currentActivity;
        $this->lastActivity = $lastActivity;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getStudentName(): String{
        return $this->studentName;
    }

    public function getStudentId(): int{
        return $this->id;
    }

    public function getItinerary(): Itinerary{
        return $this->itinerary;
    }

    public function getCurrentActivity(): Activity{
        return $this->currentActivity;
    }

    public function getCurrentActivityId(): int{
        return $this->currentActivity->getId();
    }

    public      function getLastActivity(): Activity{
        return $this->lastActivity;
    }

    public function setLastActivity(Activity $activity): void{
        $this->lastActivity = $activity;
    }

    public function nextActivity(string $answer, int $timeDoingExerciseInSecs): void{

        $nextAct = $this->itinerary->nextActivity($this->currentActivity, $answer, $timeDoingExerciseInSecs, $this->getLastActivity());

        if($this->getItinerary()->getTag($nextAct) > $this->getItinerary()->getTag($this->getCurrentActivity())){
            $this->setLastActivity($this->getCurrentActivity());
        }

        $this->currentActivity = $nextAct;
    }
}