<?php

namespace App\Domain\Student;

interface StudentRepository
{
    public function findStudent(int $id): ?Student;

}