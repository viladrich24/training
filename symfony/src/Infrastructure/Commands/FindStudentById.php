<?php

namespace App\Infrastructure\Commands;

use App\Application\GetEvenStudent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindStudentById extends Command
{
    private GetEvenStudent $getStudent;

    public function __construct(GetEvenStudent $getStudent){
        $this->getStudent = $getStudent;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('search-student')
            ->setDescription('Search for a student with the introduced id, if exists.')
            ->setHelp('Demonstration of custom commands created by Symfony Console component.')
            ->addArgument('studentId', InputArgument::REQUIRED, 'Pass the studentId.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $studentId = $input->getArgument('studentId');
        $output->writeln(sprintf('Hello student with ID: %s', $studentId));

        try {
            $student = $this->getStudent->exec($studentId);
        }
        catch (CommandNotFoundException $e){
            return Command::FAILURE;
        }

        $studentName = $student->getStudentName();
        $studentId = $student->getStudentId();
        $output->writeln(sprintf('Student with id %s is called: %s', $studentId, $studentName));
        return Command::SUCCESS;

    }
}