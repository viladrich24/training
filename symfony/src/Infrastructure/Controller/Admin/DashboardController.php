<?php

namespace App\Infrastructure\Controller\Admin;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use App\Domain\Student\Student;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
        // you can also render some template to display a proper Dashboard
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Symfony');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home',
            'homepage');
        yield MenuItem::linkToCrud('ItineraryCrud', 'fas fa-itinerary',
            Itinerary::class);
        yield MenuItem::linkToCrud('ActivityCrud', 'fas fa-activity',
            Activity::class);
        yield MenuItem::linkToCrud('StudentCrud', 'fas fa-student',
            Student::class);
    }
}
