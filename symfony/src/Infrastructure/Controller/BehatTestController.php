<?php

namespace App\Infrastructure\Controller;

use App\Domain\Student\StudentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class BehatTestController extends AbstractController
{
    private StudentRepository $studentRepository;
    private GetAllStudentInfoController $getAllStudentInfo;

    public function __construct(StudentRepository $studentRepository, GetAllStudentInfoController $getAllStudentInfo){
        $this->studentRepository = $studentRepository;
        $this->getAllStudentInfo = $getAllStudentInfo;
    }

    /**
     * @Route("/behatTest/{id}", name="behatTest", methods={"GET"})
     */
    public function index(string $id): Response{

        $student = $this->studentRepository->findStudent(intval($id));

        if(!isset($student)) {
            throw new HttpException(404, "File Not Found");
        }

        $activities = [];
        $activities = $this->getAllStudentInfo->getActivities($student, $activities);

        $studentInfo = [
            "StudentName"=>$student->getStudentName(),
            "CurrentActivityId"=>$student->getCurrentActivity()->getId(),
            "CurrentActivityName"=>$student->getCurrentActivity()->getName()
        ];

        return $this->render('/base.html.twig', [
            'controller_name' => 'BehatTestController',
            'name' => 'Dani Ruiz',
            'occupation' => 'Frontend Developer',
            'id' => $id,
            'info' => $studentInfo,
            'activities' => $activities
        ]);
    }
}