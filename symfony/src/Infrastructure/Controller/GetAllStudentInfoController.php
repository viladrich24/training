<?php

namespace App\Infrastructure\Controller;

use App\Application\GetEvenStudent;
use App\Domain\Student\Student;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetAllStudentInfoController extends AbstractController
{
    private GetEvenStudent $getStudent;

    public function __construct(GetEvenStudent $getStudent){
        $this->getStudent = $getStudent;
    }

    /**
     * @Route("/student/{id}/showStudent", name="getAllStudentInfo", methods={"GET"})
     */
    public function index(int $id): JsonResponse{

        $student = $this->getStudent->exec($id);

        if(!isset($student)){
            throw new HttpException(404, "Student Not Found because is Odd");
        }

        $activities = [];
        $activities = $this->getActivities($student, $activities);

        return new JsonResponse([
            "StudentName"=>$student->getStudentName(),
            "ItineraryId"=>$student->getItinerary()->getId(),
            "ItineraryName"=>$student->getItinerary()->getItineraryName(),
            "CurrentActivityId"=>$student->getCurrentActivity()->getId(),
            "CurrentActivityName"=>$student->getCurrentActivity()->getName(),
            "Activities" => $activities
        ]);
    }

    public function getActivities(Student $student, array $activities): array
    {
        foreach ($student->getItinerary()->getActivities() as $act) {
            $activities[$act->getName()]["activityId"] = $act->getId();
            $activities[$act->getName()]["activityName"] = $act->getName();
            $activities[$act->getName()]["position"] = $act->getPosition();
            $activities[$act->getName()]["difficulty"] = $act->getDifficulty();
            $activities[$act->getName()]["time"] = $act->getTime();
            $activities[$act->getName()]["solutions"] = $act->getSolutions();
        }
        return $activities;
    }
}