<?php

namespace App\Infrastructure\Controller;

use App\Domain\Student\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NextActivityController extends AbstractController{

    private StudentRepository $studentRepository;
    public function __construct(StudentRepository $studentRepository){
        $this->studentRepository = $studentRepository;
    }

    /**
     * @Route("/student/nextActivity", name="nextActivity", methods={"POST"})
     */
    public function index(Request $request): JsonResponse{

        $id = $request->get('id');
        $student = $this->studentRepository->findStudent($id);
        $currentAct = $student->getCurrentActivityId();

        $answer = $request->get('answer');
        $time = $request->get('time');
        $student->nextActivity($answer, $time);

        return new JsonResponse([
            "ActualActivity:", $currentAct,
            "NextActivity:", $student->getCurrentActivityId()
        ]);
    }
}

