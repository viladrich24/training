<?php

namespace App\Infrastructure\Repository;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;

class FakeStudentRepository implements StudentRepository {

    public function findStudent(int $id): ?Student{

        $activity1 = new Activity(1, "Act1", 1, 1, 30, "1_0_2");
        $activity2 = new Activity(2, "Act2", 1, 2, 40, "3_1");
        $activity3 = new Activity(3, "Act3", 1, 2, 70, "4_1_0");
        $activity4 = new Activity(4, "Act4", 1, 3, 20, "2_1_3");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4];
        $it = new Itinerary(1, "it1", $activitiesArray);

        return new Student(1, "Arnau", $it, $activity2, $activity1);
    }
}