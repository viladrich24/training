<?php

namespace App\Infrastructure\Repository;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use App\Domain\Student\Student;
use App\Domain\Student\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;

class MySQLStudentRepository implements StudentRepository {

    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findStudent(int $id): ?Student{

        $qb = $this->em->getConnection()->createQueryBuilder();
        $activitiesArray = [];

        $qb->select('s.id, s.student_name AS student_name, s.current_activity_id AS current_activity_id, s.itinerary_id AS itinerary_id, i.name AS itinerary_name, a.id AS activity_id, a.name AS activity_name, a.position, a.difficulty, a.time, a.solutions')
            ->from('student', 's')
            ->join('s', 'itinerary', 'i','i.id = s.itinerary_id')
            ->join('s', 'itinerary_activities', 'ia', 'i.id = ia.itinerary_id')
            ->join('s','activity', 'a', 'ia.activity_id = a.id')
            ->where('s.id = :identifier')
            ->setParameter('identifier', $id);

        $statement = $qb->executeQuery();
        $results = $statement->fetchAllAssociative();

        if(empty($results)){
            return null;
        }

        $itineraryId = $results[0]['itinerary_id'];
        $itineraryName = $results[0]['itinerary_name'];
        $studentName = $results[0]['student_name'];
        $currentActivity = $results[0]['current_activity_id'];

        foreach($results as $row){
            $activitiesArray[] = new Activity($row['activity_id'], $row['activity_name'], $row['position'], $row['difficulty'], $row['time'], $row['solutions']);
        }

        $it = new Itinerary($itineraryId, $itineraryName, $activitiesArray);
        return new Student($id, $studentName, $it, $it->getActivity($currentActivity), $it->getActivity($currentActivity));

    }
}