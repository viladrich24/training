<?php

declare(strict_types=1);

namespace App\Tests\Acceptance\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use http\Env\Url;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
final class DemoContext implements Context
{
    /** @var KernelInterface */
    private $kernel;

    /** @var Response|null */
    private $response;

    private int $result;
    private int $firstNum;
    private int $secondNum;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When a demo scenario sends a request to :path
     */
    public function aDemoScenarioSendsARequestTo(string $path): void
    {
        $this->response = $this->kernel->handle(Request::create($path, 'GET'));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived(): void
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }

    /**
     * @When I do something with number :arg1
     */
    public function isItANumber($arg1)
    {
        if(!is_int($arg1)){
            throw new PendingException('arg must be an Integer');
        }
    }

    /**
     * @When I do something with string :arg1
     */
    public function isItAString($arg1)
    {
        if(!is_string($arg1)){
            throw new PendingException('arg must be a String');
        }
    }

    /**
     * @Given the first number is :arg1
     */
    public function saveFirstNumber($arg1){
        $this->firstNum = $arg1;
    }

    /**
     * @Given the second number is :arg2
     */
    public function saveSecondNumber($arg2){
        $this->secondNum = $arg2;
    }

    /**
     * @When I multiply them
     */
    public function multiplyNumbers()
    {
        $this->result = $this->firstNum * $this->secondNum;
    }

    /**
     * @When I add them
     */
    public function addNumbers()
    {
        $this->result = $this->firstNum + $this->secondNum;
    }

    /**
     * @Then the result should be :arg
     */
    public function getResult($arg)
    {
        if($this->result != $arg){
            throw new PendingException('The result is not correct');
        }
    }

    /**
     * @Given I have the initial table:
     */
    public function thisTableIsFullOfStrings(TableNode $table){

        foreach ($table as $row) {
            var_dump($row);
        }
    }

    /**
     * @Given I am in /tests folder
     */
    public function moveIntoTestDir()
    {
        if (!is_dir('tests')) {
            throw new PendingException('You are not in the /tests folder');
        }
    }

    /**
     * @Then I should see this :dir
     */
    public function checkDir($dir)
    {
        if (!is_dir("tests/".$dir)) {
            throw new PendingException('You are not in the /tests folder');
        }
    }

    /**
     * @Given I have a file named :filename
     */
    public function iHaveAFileNamed($filename)
    {
        touch($filename);
    }

    /**
     * @When I have this StudentId :StudentId
     */
    public function completeURL($StudentId): string
    {
        return "http://localhost:8001/".$StudentId."/showStudent";
    }
}
