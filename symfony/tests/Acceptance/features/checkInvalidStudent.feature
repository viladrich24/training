Feature:
  As a user
  I want to have a demo scenario into: behatTestController
  In order to prove that it checks for a correct and valid StudentId

  Scenario: Check if studentId exists
    Given I am on "/behatTest/1"
    Then the response status code should be 200
    Then I should see "StudentName: Marcos"

  Scenario: Check if studentId does not exist
    Given I am on "/behatTest/6"
    Then the response status code should be 404
