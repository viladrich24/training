Feature:
  In order to see the directory structure
  As a UNIX user
  I need to be able to list the current directory's contents

  Scenario: List files in a directory
    Given I am in /tests folder
    And I have a file named "bootstrap.php"
    Then I should see this Acceptance
    And I should see this Unit