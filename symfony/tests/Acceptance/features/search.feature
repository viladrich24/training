Feature:
  In order to prove that the Behat Symfony extension is correctly installed
  As a user
  I want to have a demo scenario

  Scenario: Search text in localhost home page
    Given I am on "/"
    Then I should see "Welcome to Symfony"

  Scenario: Search data in the JSON response
    Given I am on "/student/Behat"
    Then the response status code should be 200
    And the JSON should be equal to:
    """
      {
        "StudentName":"Maria"
        "ItineraryId":1
        "ItineraryName":"itinerary1"
        "CurrentActivityId":2
        "CurrentActivityName":"Act2"
      }
    """

 # Scenario: Search text in localhost home page
 #   Given I am on "/"
 #   And I send a "GET" request to "/student/1/showStudent"
 #   Then the response status code should be 200



