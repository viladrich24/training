Feature:
  In order to prove that the Behat Symfony extension is correctly installed
  As a user
  I want to have a demo scenario

  Background:
    Given the first number is 10
    And the second number is 3

  Scenario: Check if background works correctly in multiplication
    When I multiply them
    Then the result should be 30

  Scenario: Check if background works correctly in addition
    When I add them
    Then the result should be 13
