Feature:
  In order to find the current Activity a student is doing
  As a teacher
  I need to be able to search for its current_activity_id

  Scenario Outline: Check if the multiplication is correctly done
    Given the first number is "<n1>"
    And the second number is "<n2>"
    When I multiply them
    Then the result should be "<n3>"

    Examples:
      | n1 | n2 | n3 |
      | 5  | 6  | 30 |
      | 4  | 3  | 12 |


#  Scenario Outline: Check if URL is correctly entered
#    Given I am on "/student"
#    When I have this StudentId "<id>"
#    Then I should see "<name>"
#
#    Examples:
#      | id | name |
#      | 1  | Pepe |
#      | 2  | Marcos |