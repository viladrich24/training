Feature:
  In order to prove that the Behat Symfony extension is correctly installed
  As a user
  I want to have a demo scenario

  Scenario: Check if it is a string
    When I do something with string 'hola'

  Scenario: Check if it is an integer
    When I do something with number 13

  Scenario: Check if the multiplication is correctly done
    Given the first number is 5
    And the second number is 7
    When I multiply them
    Then the result should be 35

  Scenario: Check if the sum is correctly done
    Given the first number is 5
    And the second number is 7
    When I add them
    Then the result should be 12

  Scenario: Get a table and print it
    Given I have the initial table:
      | Hola | gran | weiss |
      | bon | petit | bier |
      | dia | klein | gross |

