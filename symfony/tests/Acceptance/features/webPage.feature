Feature:
  As a user
  I want to have a demo scenario: Search text webpage
  In order to prove that the Behat Symfony extension is correctly installed

  Scenario: Search text webpage
    Given I am on "/behatTest"
    Then I should see "Fent proves amb Behat"

