<?php

namespace Unit\Entity;

use App\Domain\Activity\Activity;
use App\Domain\Activity\InvalidNumberOfAnswersException;
use PHPUnit\Framework\TestCase;

class ActivityTest extends TestCase
{
    /**
     * @test
     * @dataProvider scoreProvider
     */
    public function validate_score_function(string $answer, int $expectedScore)
    {
        $activity = new Activity(1, "Act1", 1, 2, 30, "1_0_2");
        $this->assertEquals($expectedScore, $activity->getScore($answer));
    }

    public function scoreProvider(){
        return [
            ['1_1_0', 33],
            ['1_0_2', 100],
            ['4_3_6', 0]
        ];
    }

    /**
     * @test
     */
    public function avoid_out_of_range_difficulty()
    {
        $this->expectException(\InvalidArgumentException::class);
        $activity = new Activity(1, "Act1", 1, 11, 30, "1_0_2");
    }

    /**
     * @test
     * @dataProvider provider
     */
    public function avoid_answer_different_length_than_solutions(string $answer)
    {
        $this->expectException(InvalidNumberOfAnswersException::class);
        $activity = new Activity(1, "Act1", 1, 2, 30, "1_0_2");
        $activity->getScore($answer);
    }

    public function provider(){
        return [
            ['1_2'],
            ['1_2_3_4']
        ];
    }

}
