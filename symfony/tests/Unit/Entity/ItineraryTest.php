<?php

namespace Unit\Entity;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use PHPUnit\Framework\TestCase;

class ItineraryTest extends TestCase
{

    /**
     * @test
     */
    public function avoid_adding_empty_array()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("ActivitiesArray is Empty");
        new Itinerary(1, "it1", []);
    }

    /**
     * @test
     */
    public function check_activity_initialization_test()
    {
        $activity1 = new Activity(1, "Act1", 1, 1, 30, "1_0_2");
        $activity2 = new Activity(2, "Act2", 1, 2, 40, "3_1");
        $activity3 = new Activity(3, "Act3", 1, 2, 70, "4_1_0");
        $activity4 = new Activity(4, "Act4", 1, 3, 20, "2_1_3");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4];
        $itinerary = new Itinerary(1, "it1", $activitiesArray);

        $this->assertEquals(1, $itinerary->getId());
    }

    /**
     * @test
     */
    public function check_bad_writing_in_activity_initialization_test()
    {
        $activity1 = new Activity(1, "Act1", 1, 1, 30, "1_0_2");
        $activity2 = new Activity(2, "Act2", 1, 2, 40, "3_1");

        $activitiesArray = [$activity1, $activity2, 123];
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("ActivitiesArray is not filled with Activities");
        new Itinerary(1, "it1", $activitiesArray);

    }

    /**
     * @test
     */
    public function check_next_activity_method_great_score_great_time()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "2_4_1");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "2_4_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];

        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $nextAct = $itinerary->nextActivity($activity8, "2_4_1", 10, $activity8);
        $this->assertEquals(5, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity5, "2_4_1", 10, $activity8);
        $this->assertEquals(7, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity2, "2_4_1", 10, $activity4);
        $this->assertEquals(2, $nextAct->getId());

    }

    /**
     * @test
     */
    public function check_next_activity_method_great_score_bad_time()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "2_4_1");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "2_4_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];

        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $nextAct = $itinerary->nextActivity($activity8, "2_4_1", 100, $activity8);
        $this->assertEquals(1, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity1, "2_4_1", 100, $activity8);
        $this->assertEquals(4, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity2, "2_4_1", 100, $activity4);
        $this->assertEquals(2, $nextAct->getId());

    }

    /**
     * @test
     */
    public function check_next_activity_method_bad_score()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "2_4_1");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "2_4_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];

        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $nextAct = $itinerary->nextActivity($activity8, "0_0_0", 100, $activity8);
        $this->assertEquals(8, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity6, "0_0_0", 10, $activity1);
        $this->assertEquals(4, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity7, "0_0_0", 10, $activity3);
        $this->assertEquals(6, $nextAct->getId());

        $nextAct = $itinerary->nextActivity($activity2, "0_0_0", 100, $activity5);
        $this->assertEquals(3, $nextAct->getId());

    }

}
