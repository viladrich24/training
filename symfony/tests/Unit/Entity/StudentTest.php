<?php

namespace Unit\Entity;

use App\Domain\Activity\Activity;
use App\Domain\Itinerary\Itinerary;
use App\Domain\Student\Student;
use PHPUnit\Framework\TestCase;

class StudentTest extends TestCase
{

    /**
     * @test
     */
    public function create_student_successfully()
    {
        $activity1 = new Activity(1, "Act1", 1, 1, 30, "1_0_2");
        $activity2 = new Activity(2, "Act2", 1, 2, 40, "3_1");
        $activitiesArray = [$activity1, $activity2];
        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $student = new Student(1, "Arnau", $itinerary, $activity2, $activity1);

        $currentStudentAct = $student->getCurrentActivityId();

        $this->assertEquals(2, $currentStudentAct);
    }


    /**
     * @test
     */
    public function check_next_activity_function_given_great_score_option_0()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "1_0_2");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "3_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];
        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $student = new Student(1, "Josep", $itinerary, $activity1, $activity3);

        $initialStudentAct = $student->getCurrentActivityId();
        $this->assertEquals(1, $initialStudentAct);

        $nextAct = $itinerary->nextActivity($student->getCurrentActivity(), "2_4_1", 10, $student->getLastActivity());

        $this->assertEquals(5, $nextAct->getId());
    }

    /**
     * @test
     */
    public function check_next_activity_function_given_great_score_bad_time_option_1()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "1_0_2");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "3_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];
        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $student = new Student(1, "Josep", $itinerary, $activity1, $activity3);

        $initialStudentAct = $student->getCurrentActivityId();
        $this->assertEquals(1, $initialStudentAct);

        $nextAct = $itinerary->nextActivity($student->getCurrentActivity(), "2_4_1", 100, $student->getLastActivity());

        $this->assertEquals(4, $nextAct->getId());

    }

    /**
     * @test
     */
    public function check_next_activity_function_given_bad_score_option_2()
    {
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "1_0_2");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "3_1");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];
        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $student = new Student(1, "Josep", $itinerary, $activity3, $activity1);

        $initialStudentAct = $student->getCurrentActivityId();
        $this->assertEquals(3, $initialStudentAct);

        $nextAct = $itinerary->nextActivity($student->getCurrentActivity(), "0_1_0", 100, $student->getLastActivity());

        $this->assertEquals(4, $nextAct->getId());
    }

    /**
     * @test
     */
    public function update_current_activity_to_student()
    {
        $activity8 = new Activity(8, "Act8", 3, 1, 50, "2_4_1");
        $activity1 = new Activity(1, "Act1", 5, 1, 50, "2_4_1");
        $activity4 = new Activity(4, "Act4", 7, 1, 30, "1_0_2");
        $activity5 = new Activity(5, "Act5", 1, 2, 50, "2_4_1");
        $activity3 = new Activity(3, "Act3", 3, 2, 50, "2_4_1");
        $activity6 = new Activity(6, "Act6", 4, 2, 40, "3_1");
        $activity7 = new Activity(7, "Act7", 1, 3, 50, "2_4_1");
        $activity2 = new Activity(2, "Act2", 8, 3, 50, "2_4_1");
        $activitiesArray = [$activity1, $activity2, $activity3, $activity4, $activity5, $activity6, $activity7, $activity8];
        $itinerary = new Itinerary(1,  "it1", $activitiesArray);
        $student = new Student(1, "Josep", $itinerary, $activity1, $activity3);

        $this->assertEquals(1, $student->getCurrentActivityId());
        $this->assertEquals(3, $student->getLastActivity()->getId());

        $student->nextActivity("2_4_1", 10);

        $this->assertEquals(5, $student->getCurrentActivityId());
        $this->assertEquals(1, $student->getLastActivity()->getId());
    }

}